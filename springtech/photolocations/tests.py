from django.test import TestCase

from .models import Location
from .forms import LocationForm

# Note: I split this task up into *two* tests actually, since
# "successfully saved to the database" and "validation error when
# submitted without a name" are somewhat conceptually different.
class LocationModelFormTests(TestCase):
    """Write a test, which tests that the location form successfully
    results in a location being saved to the database when used
    correctly but returns a validation error when submitted without
    a name.
    """
    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        cls.form_data_valid = {
            'name': 'Amazing Location',
            'location_type': 'drillhole',
            'notes': 'The best location to drill a hole.', 
        }
        cls.form_data_no_name = {
            'name': '',
            'location_type': 'drillhole',
            'notes': 'The best location to drill a hole.', 
        }

    def test_location_form_save_successful(self):
        """Validate that valid form data results in a new location
        being saved to the database successfully.
        """
        location_form = LocationForm(self.form_data_valid)
        new_location = location_form.save()
        # test that a valid primary key (int) is returned from the DB
        self.assertIs(type(new_location.pk), int) 

    def test_location_form_name_validation(self):
        """Validate that passing no name to the LocationForm raises
        a validation error upon submitting.
        """
        location_form = LocationForm(self.form_data_no_name)
        # form data should *not* be valid
        self.assertFalse(location_form.is_valid())
