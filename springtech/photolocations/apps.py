from django.apps import AppConfig


class PhotolocationsConfig(AppConfig):
    name = 'photolocations'
