from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views import generic

from .forms import LocationForm, PhotoFormSet
from .models import Location, Photo

def index(request):
    return redirect('all_locations')

# Note that create_location is the only view which isn't a generic CBV:
# In this case it was simpler to create, add and process the forms in
# the view manually, whereas the others are all generic CBVs because
# the logic was generic enough to encase in them.
def create_location(request):
    """Write a view to create a new Location.
    In this view, the user should have the ability to save a location
    and upload three photos to the location.
    """
    if request.method == 'POST': # submitted form
        location_form = LocationForm(request.POST)
        photo_formset = PhotoFormSet(request.POST, request.FILES, prefix='photo_set')
        if location_form.is_valid() and photo_formset.is_valid():
            # Save the location first, so the photos have a valid location to refer to
            new_location = location_form.save()
            # Return the valid photos, without commiting them to the DB:
            photos = photo_formset.save(commit=False) 
            for p in photos:
                # And save them only once we've set their location :)
                p.location = new_location
                p.save()
        return redirect('location_details', new_location.id)
    else: # loading the page
        location_form = LocationForm()
        photo_formset = PhotoFormSet(instance=None, prefix='photo_set')

    context = {
        'location_form': location_form,
        'photo_formset': photo_formset,
    }
    return render(request, 'photolocations/create_location.html', context)

class EditLocationView(generic.UpdateView):
    """Write a view to edit an existing Location.
    This view doesn't need to enable editing of the photos, just the
    location itself.
    """
    model = Location
    form_class = LocationForm
    template_name = 'photolocations/edit_location.html'

class AllLocationsView(generic.ListView):
    """Write a view to list the existing locations, with a table
    showing the name, location type, and count of photos for each item.
    """
    template_name = 'photolocations/all_locations.html'
    context_object_name = 'locations'

    def get_queryset(self):
        # annotate() adds to the query;
        # Count() counts the number of photo objects which have this given
        # location as its foreign key
        queryset = Location.objects.annotate(photo_count=Count('photo')).order_by('-datetime_saved')
        return queryset

class LocationDetailView(generic.DetailView):
    """Write a view to see the detail of a single location,
    showing the photos.
    """
    model = Location
    template_name = 'photolocations/location_details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # update context with the photos at this location
        context['photos'] = Photo.objects.filter(location=self.object)
        return context
