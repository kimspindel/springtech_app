from django.forms import inlineformset_factory, ModelForm
from .models import Location, Photo

PhotoFormSet = inlineformset_factory(Location, Photo, fields=('image_file',), extra=3, can_delete=False)

class LocationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            # for Bootstrap forms
            visible.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Location
        fields = [
            'name',
            'location_type',
            'notes',
        ]
