from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('locations/', views.AllLocationsView.as_view(), name='all_locations'),
    path('locations/create/', views.create_location, name='create_location'),
    path('locations/edit/<int:pk>/', views.EditLocationView.as_view(), name='edit_location'),
    path('locations/details/<int:pk>/', views.LocationDetailView.as_view(), name='location_details'),
] 
