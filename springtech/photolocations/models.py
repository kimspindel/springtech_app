from django.db import models
from django.urls import reverse

class Location(models.Model):
    DRILL_HOLE = 'drillhole'
    GEOLOGY_NOTE = 'geonote'
    PHOTO = 'photo'

    name = models.CharField(max_length=100)
    # The location type may as well be a small int field which would take
    # up less space in the db, but of course be less readable :)
    location_type = models.CharField(
        max_length=9,
        choices=(
            (DRILL_HOLE, 'Drill Hole'),
            (GEOLOGY_NOTE, 'Geology Note'),
            (PHOTO, 'Photo'),
        )
    )
    notes = models.TextField(blank=True)
    # wasn't 100% sure if the timestamp should be updated upon saving;
    # if not then it should be 'auto_now_add=True' instead
    datetime_saved = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('location_details', args=[str(self.id)])

class Photo(models.Model):
    image_file = models.ImageField(upload_to='uploads/%Y/%m/%d/')
    location = models.ForeignKey(Location, null=True, on_delete=models.SET_NULL)
